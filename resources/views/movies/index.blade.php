@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if(count($movies) > 0)
                <div class="card-body p-0">
                    <table class="table table-hover m-0">
                        <thead>
                            <tr>
                                <th scope="col" class="border-top-0">Tile</th>
                                <th scope="col" class="border-top-0">Active</th>
                                <th scope="col" class="border-top-0"></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach($movies as $movie)
                            <tr>
                                <td class="align-middle">{{ $movie->title }}</td>
                                <td class="align-middle">{{ $movie->active }}</td>
                                <td class="text-right">
                                    <a href="{{ route('movies.edit', $movie) }}" class="btn btn-success btn-sm mr-1">Edit</a>
                                    <a href="{{ route('movies.destroy', $movie) }}" class="btn btn-danger btn-sm">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                @else
                    <div class="card-body text-center">No movies found.</div>
                @endif
            </div>
        </div>
    </div>
@endsection
