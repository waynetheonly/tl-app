<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $timestamp = Carbon::now();

        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin',
            'email_verified_at' => $timestamp,
            'password' => bcrypt('secret'),
            'created_at' => $timestamp,
            'updated_at' => $timestamp,
        ]);
    }
}
