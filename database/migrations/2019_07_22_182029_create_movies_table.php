<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('distributor_id');
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('genre')->nullable();
            $table->date('release_date')->nullable();
            $table->float('rating')->nullable();
            $table->string('directors')->nullable();
            $table->string('actors')->nullable();
            $table->time('duration')->nullable();
            $table->text('image')->nullable();
            $table->boolean('active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
