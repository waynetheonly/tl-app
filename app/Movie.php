<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    public function distributor()
    {
        return $this->hasOne(Distributor::class);
    }
}
